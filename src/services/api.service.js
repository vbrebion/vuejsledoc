import axios from '../config/axios'

// Connexion
const login = (identifier, password) => {return axios.post('/auth/local', {identifier, password})}
const register = (username, email, password) => {return axios.post('/auth/local/register', {username, email, password})}
const forgotPassword = (email) => {return axios.post('/auth/forgot-password', {email})}

// Déconnexion
const disconnect = () => {
    localStorage.clear();
};

//patient
const CreationPatient = (data) => {return axios.post('/patients', { data: data })}
const deletePatient = (id) => { return axios.delete(`/patients/${id}`) };
export default { login, register, forgotPassword, disconnect,CreationPatient,deletePatient }
