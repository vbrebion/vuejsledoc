import { ref } from 'vue';
import apiService from '@/services/api.service';

export default function useRegisters() {
    const apiErrorRegister = ref('');

    async function register(registerUsername, registerMail, RegisterPassword) {
        try {
            const response = await apiService.register(registerUsername, registerMail, RegisterPassword);
            window.location.reload();
            console.log(response);
        } catch(error) {
            if (error.response && error.response.data && error.response.data.error) {
                apiErrorRegister.value = error.response.data.error.message;
            } else {
                apiErrorRegister.value = "Une erreur s'est produite lors du traitement de votre demande.";
            }
        }
    }
    
    return {
        register,
        apiErrorRegister
    };
}