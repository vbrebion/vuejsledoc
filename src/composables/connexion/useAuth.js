import { ref } from 'vue';
import { useRouter } from 'vue-router'
import apiService from '@/services/api.service';

export default function useAuth() {
  const router = useRouter(); // Initialize the router
    const apiError = ref('');

    async function login(Connexionemail, Connexionpassword) {
      try {
          const response = await apiService.login(Connexionemail, Connexionpassword);
          localStorage.setItem('AUTH_TOKEN', response.data.jwt);
          localStorage.setItem('user', JSON.stringify(response.data.user));
          router.push({ name: 'Dashboard' });
          console.log(response);
      } catch(error) {
          if (error.response && error.response.data && error.response.data.error) {
              apiError.value = error.response.data.error.message;
          } else {
              apiError.value = "Une erreur s'est produite lors du traitement de votre demande.";
          }
      }
  }
  

    return {
        login,
        apiError
    };
}
