import { ref } from 'vue'
import  Schema  from 'async-validator'

export default function useForm(options) {
    const formErrors = ref({})
    const form = ref(options.form)
    const validator = new Schema(options.validator)

    function handleErrors(errors) {
      errors.forEach((error) => {
        formErrors.value[error.field] = error.message
      })
    }
  
    function validate() {
      return new Promise((resolve, reject) => {
        validator.validate(form.value, (errors, fields) => {
          formErrors.value = {}
          if (errors) {
            handleErrors(errors)
            reject(errors)
          } else {
            resolve()
          }
        })
      })
    }
    
    return {
      formErrors,
      form, 
      validator,
      validate
    }
}
