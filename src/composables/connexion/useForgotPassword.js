import { ref, defineEmits } from 'vue';
import apiService from '@/services/api.service';

export default function useForgotPassword() {
    const apiErrorForgot = ref('');
    const emit = defineEmits(['close'])
    async function forgotPassword(forgotEmail) {
        try {
            const response = await apiService.forgotPassword(forgotEmail);
            console.log(response);
            emit('close');
        } catch(error) {
            apiErrorForgot.value = error.response.ok
        }
    }
    return {
        forgotPassword,
        apiErrorForgot
    };
}