import { createRouter, createWebHashHistory } from 'vue-router'

const Login = () => import('../src/views/LoginView.vue')
const Dashboard = () => import('../src/views/DashboardView.vue')
const PatientsView = () => import('../src/views/PatientsView.vue')
const ControleView = () => import('../src/views/ControleView.vue')
const MeetingView = () => import('../src/views/MeetingView.vue')

export const router = createRouter({
	history: createWebHashHistory(),
  routes: [
    {
        path: '/',
        name: 'login',
        component: Login
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      children: [
        {
          path: '/Controle',
          name: 'Controle',
          component: ControleView
        },
        {
          path: '/Patients',
          name: 'Patients',
          component: PatientsView
        },
        {
          path: '/Meeting',
          name: 'Meeting',
          component: MeetingView
        }
      ]
    }
  ]
})

export default router;