import { defineStore } from 'pinia'

export const useStore = defineStore({
  id: 'main',
  state: () => ({
    currentTab: 'Home'
  }),
  actions: {
    setCurrentTab(tab) {
      this.currentTab = tab
    }
  }
})