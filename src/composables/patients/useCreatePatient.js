import { ref } from 'vue';
import { useRouter } from 'vue-router'
import apiService from '@/services/api.service';

export default function useCreatePatient() {
    const router = useRouter(); // Initialize the router
    const apiError = ref('');

    async function CreatePatient(object) {
        try {
            const response = await apiService.CreationPatient(object);
            router.push({ name: 'Patients' });
            console.log(response);
        } catch(error) {
            if (error.response && error.response.data && error.response.data.error) {
                apiError.value = error.response.data.error.message;
            } else {
                apiError.value = "Une erreur s'est produite lors du traitement de votre demande.";
            }
        }
    }
    return {
        CreatePatient,
        apiError
    };
}